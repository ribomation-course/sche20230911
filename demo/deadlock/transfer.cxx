#include <iostream>
#include <string>
#include <syncstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <utility>
#include <chrono>

using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;
using std::osyncstream;
using std::string;

struct Account {
    std::mutex exclusive{};
    const string accno;
    int balance;
    Account(string accno, int balance)
            : accno(std::move(accno)), balance(balance) {}
};
auto operator <<(std::ostream& os, Account const& a) -> std::ostream& {
    return os << "{" << a.accno << ", " << a.balance << " kr}";
}

struct Updater {
    unsigned id;
    Account& from;
    Account& to;
    int amount;
    Updater(unsigned id, Account& from, Account& to, int amount)
            : id(id), from(from), to(to), amount(amount) {}
    [[noreturn]] void operator ()() {
        do {
            osyncstream{cout} << "updater-" << id << " before transfer\n";
            {
                auto guard = std::scoped_lock{from.exclusive, to.exclusive};
                from.balance -= amount;
                to.balance += amount;
                osyncstream{cout} << "updater-" << id << " transferred "
                                  << amount << " kr: " << from << " --> " << to << "\n";
            }
        } while (true);
    }
};

int main() {
    auto account_1 = Account{"1111"s, 100};
    auto account_2 = Account{"2222"s, 200};
    auto updater_1 = std::jthread{Updater{1, account_1, account_2, 100}};
    auto updater_2 = std::jthread{Updater{2, account_2, account_1, 50}};
}

