cmake_minimum_required(VERSION 3.22)
project(deadlock)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(kitchen kitchen.cxx)
target_compile_options(kitchen PRIVATE ${WARN})

add_executable(transfer transfer.cxx)
target_compile_options(transfer PRIVATE ${WARN})


