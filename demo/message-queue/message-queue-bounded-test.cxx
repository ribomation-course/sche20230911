#include <iostream>
#include <thread>
#include <syncstream>
#include "message-queue-bounded.hxx"

using ribomation::concurrent::MessageQueueBounded;
using std::cout;
using std::osyncstream;

int main() {
    auto N = 100'000L;
    auto q = MessageQueueBounded<long, 8U>{};

    auto consumer = std::jthread{[&q] {
        auto sum = 0L, count = 0L;
        for (auto msg = q.get(); msg > 0; msg = q.get()) {
            sum += msg;
            ++count;
        }
        osyncstream{cout} << "[consumer] count=" << count << ", sum=" << sum << "\n";
    }};

    for (auto msg = 1L; msg <= N; ++msg) q.put(msg);
    q.put(0L);
    osyncstream{cout} << "[producer] sum=" << (N * (N + 1) / 2) << "\n";
}

