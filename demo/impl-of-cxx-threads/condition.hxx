#pragma once
#include <functional>
#include <pthread.h>
#include "mutex.hxx"

namespace ribomation::concurrent {

    class Condition {
        mutable pthread_cond_t condition{};
        Mutex& mutex;
    public:
        explicit Condition(Mutex& mutex) : mutex(mutex) {
            pthread_cond_init(&condition, nullptr);
        }
        ~Condition() {
            pthread_cond_destroy(&condition);
        }

        void wait(std::function<bool()> const& ok_to_proceed) {
            while (!ok_to_proceed()) {
                pthread_cond_wait(&condition, mutex.native());
            }
        }
        void notify() { pthread_cond_signal(&condition); }
        void notifyAll() { pthread_cond_broadcast(&condition); }
    };

}


