#include <iostream>
#include <unistd.h>
#include "thread.hxx"
using std::cout;
namespace rm =  ribomation::concurrent;

int main() {
    cout << "[main] enter\n";
    {
        auto t1 = rm::Thread{[] {
            cout << "[thread] hello from a lambda thread\n";
            sleep(1);
            cout << "[thread] hello again\n";
        }};
        auto t2 = rm::Thread{[] {
            cout << "[thread] hello from a 2nd thread\n";
            sleep(2);
            cout << "[thread] hello again, from #2\n";
        }};
    }
    cout << "[main] exit\n";
}


