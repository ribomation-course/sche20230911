#include "account.hxx"
#include "atm.hxx"
#include <mutex>

class ThreadSafeAccount : public Account {
    std::mutex exclusive;
public:
    void update(int amount) override {
        exclusive.lock();
        Account::update(amount);
        exclusive.unlock();
    }
};

int main(int argc, char** argv) {
    auto atm = ATM<ThreadSafeAccount>{};
    atm.args(argc, argv);
    atm.run();
}

