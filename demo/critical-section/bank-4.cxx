#include "atm.hxx"
#include <mutex>

class Account {
    std::mutex exclusive;
    int balance = 0;
public:
    int getBalance() {
        auto guard = std::lock_guard<std::mutex>{exclusive};
        return balance;
    }
    void update(int amount)  {
        auto guard = std::lock_guard<std::mutex>{exclusive};
        balance = getBalance() + amount;
    }
};

int main(int argc, char** argv) {
    auto atm = ATM<Account>{};
    atm.args(argc, argv);
    atm.run();
}

