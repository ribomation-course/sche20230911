#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <syncstream>
#include <thread>
#include <barrier>

template<typename AccountType>
class ATM {
    unsigned num_updaters = 5U;
    unsigned num_transactions = 10'000U;
    int amount = 100;

public:
    void args(int argc, char** argv) {
        using namespace std::string_literals;
        for (auto k = 1; k < argc; ++k) {
            auto arg = std::string{argv[k]};
            if (arg == "-u"s) num_updaters = std::stoi(argv[++k]);
            else if (arg == "-t"s) num_transactions = std::stoi(argv[++k]);
            else if (arg == "-a"s) amount = std::stoi(argv[++k]);
            else {
                std::cerr << "usage: " << argv[0] << " [-u <int>] [-t <int>] [-a <int>]\n";
                throw std::invalid_argument{"option "s + arg};
            }
        }
        using std::cout;
        cout << "# updaters    : " << num_updaters << "\n";
        cout << "# transactions: " << num_transactions << "\n";
        cout << "amount        : " << amount << " kr\n";
    }

    void run() {
        auto the_account = AccountType{};
        std::cout << "Initial balance: " << the_account.getBalance() << " kr\n";

        auto proceed = std::barrier{num_updaters};
        auto updater = [&the_account, &proceed, this](unsigned id) {
            using std::osyncstream, std::cout;
            proceed.arrive_and_wait();
            osyncstream{cout} << "updater-" << id << " started\n";

            for (auto k = 0U; k < num_transactions; ++k) the_account.update(+amount);
            for (auto k = 0U; k < num_transactions; ++k) the_account.update(-amount);

            proceed.arrive_and_wait();
            osyncstream{cout} << "updater-" << id << " done\n";
        };

        {
            auto updaters = std::vector<std::jthread>{};
            updaters.reserve(num_updaters);
            for (auto id = 1U; id <= num_updaters; ++id) updaters.emplace_back(updater, id);
        }
        std::cout << "Final balance: " << the_account.getBalance() << " kr\n";
    }
};


