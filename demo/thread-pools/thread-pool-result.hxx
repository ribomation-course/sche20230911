#pragma once

#include <thread>
#include <future>
#include <atomic>
#include <functional>
#include <vector>
#include "message-queue-unbounded.hxx"

namespace ribomation::concurrent {

    template<typename ResultType>
    struct Task {
        std::function<ResultType()> fn;
        std::promise<ResultType> reply{};

        Task() : fn([] { return ResultType{}; }) {}
        Task(std::function<ResultType()> fn) : fn(fn) {}
        Task(Task&& rhs)  noexcept : fn(std::move(rhs.fn)), reply(std::move(rhs.reply)) {}
    };

    template<typename ResultType>
    class ThreadPoolResult {
        std::atomic_bool done{};
        MessageQueueUnbounded<Task<ResultType>> tasks{};
        std::vector<std::jthread> threads{};

        void worker_loop() {
            do {
                auto task = tasks.get();
                if (done) break;
                try {
                    auto result = task.fn();
                    task.reply.set_value(result);
                } catch (...) {
                    task.reply.set_exception(std::current_exception());
                }
            } while (!done);
        }

        void create(unsigned num_workers) {
            try {
                for (auto k = 0U; k < num_workers; ++k)
                    threads.emplace_back(&ThreadPoolResult::worker_loop, this);
            } catch (...) { done = true; throw; }
        }

        void shutdown() {
            done = true;
            for (auto k=0UL; k<threads.size(); ++k) tasks.put(Task<ResultType>{});
            for (auto&& t: threads) if (t.joinable()) t.join();
        }

    public:
        ThreadPoolResult() : ThreadPoolResult{std::thread::hardware_concurrency()} {}
        explicit ThreadPoolResult(unsigned num_workers) { create(num_workers); }
        ~ThreadPoolResult() { shutdown(); }

        auto submit(std::function<ResultType()> fn) -> std::shared_future<ResultType> {
            auto task = Task{fn};
            auto f = task.reply.get_future().share();
            tasks.put(std::move(task));
            return f;
        }

    };

}

