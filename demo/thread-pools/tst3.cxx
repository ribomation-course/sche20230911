#include <iostream>
#include <vector>
#include <syncstream>
#include <chrono>
#include "thread-pool-result.hxx"

using namespace std::chrono_literals;
namespace rm = ribomation::concurrent;
namespace cr = std::chrono;
using std::cout;
using std::osyncstream;

int main() {
    auto const from = 100L;
    auto const step = 100L;
    auto const N    = 100L;
    auto const S    = .25s;
    auto startTime  = cr::high_resolution_clock::now();
    {
        auto args = std::vector<long>(N);
        auto sums = std::vector<std::shared_future<long>>(N);
        auto pool = rm::ThreadPoolResult<long>{};
        cout << "[main] before tasks\n";
        auto n = from;
        for (auto k = 0L; k < N; ++k, n += step) {
            args[k] = n;
            sums[k] = pool.submit([n, S] {
                std::this_thread::sleep_for(S);
                return n * (n + 1) / 2;
            });
        }
        cout << "[main] before replies\n";
        {
            auto arg = from;
            for (auto&& f: sums) {
                cout << "SUM(" << arg << ") = " << f.get() << "\n";
                arg += step;
            }
        }
        cout << "[main] before shutdown\n";
    }
    auto endTime = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "[main] elapsed   " << elapsed.count() << " ms\n";

    auto T = std::thread::hardware_concurrency();
    auto estimated = cr::duration_cast<cr::milliseconds>(S * N / T);
    cout << "[main] estimated " << estimated.count() << " ms\n";
}

