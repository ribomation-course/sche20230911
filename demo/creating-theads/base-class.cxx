#include <iostream>
#include <thread>
#include <syncstream>
#include <chrono>
#include <vector>

namespace cr = std::chrono;
using namespace std::chrono_literals;
using std::cout;
using std::osyncstream;

class Thread {
    std::thread runner;
protected:
    void body() { run(); }
    virtual void run() = 0;
public:
    Thread() = default;
    virtual ~Thread() { join(); }
    void start() {
        auto thr = std::thread{&Thread::body, this};
        std::swap(thr, runner);
    }
    void join() {
        if (runner.joinable()) runner.join();
    }
};

struct Hello : Thread {
    Hello() = default;
    void setId(unsigned id_) { id = id_; }
protected:
    void run() override {
        osyncstream{cout} << "[thr-" << id << "] started\n";
        std::this_thread::sleep_for(1s);
        osyncstream{cout} << "[thr-" << id << "] done\n";
    }
private:
    unsigned id;
};

int main() {
    auto start = cr::high_resolution_clock::now();
    cout << "[main] enter\n";
    {
        auto threads = std::vector<Hello>(5);
        auto id = 1U;
        for (auto&& t: threads) t.setId(id++);
        for (auto&& t: threads) t.start();
    }
    cout << "[main] exit\n";
    auto stop = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(stop - start);
    cout << "[main] elapsed " << elapsed.count() << " ms\n";
}



