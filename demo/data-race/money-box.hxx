#pragma once
#include <mutex>

class Moneybox {
    std::mutex exclusive;
    int payload = 0;
public:
    Moneybox() = default;
    ~Moneybox() = default;
    Moneybox(Moneybox const&) = delete;
    auto operator =(Moneybox const&) -> Moneybox& = delete;

    void send(int amount) {
        auto guard = std::lock_guard<std::mutex>{exclusive};
        payload = amount;
    }

    int recv() {
        auto guard = std::lock_guard<std::mutex>{exclusive};
        return payload;
    }
};

