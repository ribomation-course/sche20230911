#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>

using namespace std::string_literals;
using namespace std::chrono_literals;
namespace cr = std::chrono;
using std::cout;

int main() {
    auto start = cr::steady_clock::now();
    auto time = [&start]() {
        auto elapsed = cr::steady_clock::now() - start;
        auto time = cr::duration_cast<cr::duration<float, std::ratio<1, 1000>>>(elapsed);
        auto buf = std::ostringstream{};
        buf << std::fixed << std::setprecision(2) << time.count() << "ms "s;
        return buf.str();
    };

    cout << time() << "[main] enter\n";
    auto thr = std::jthread{[&time](std::stop_token stopToken) {
        cout << time() << "[thread] enter\n";
        do {
            cout << "." << std::flush;
            std::this_thread::sleep_for(.25s);
            if (stopToken.stop_requested()) {
                cout << time() << "[thread] just a small nap, zzzz\n";
                std::this_thread::sleep_for(1s);
                cout << time() << "[thread] exit\n";
                return;
            }
        } while (true);
    }};

    cout << time() << "[main] before sleep\n";
    std::this_thread::sleep_for(2s);
    cout << "\n" << time() << "[main] before request stop\n";
    thr.request_stop();
    cout << time() << "[main] before join\n";
    thr.join();
    cout << time() << "[main] after join\n";
}
