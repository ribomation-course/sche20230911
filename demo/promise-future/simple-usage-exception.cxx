#include <iostream>
#include <string>
#include <stdexcept>
#include <chrono>
#include <future>
#include <thread>

namespace cr = std::chrono;
using std::cout;
using namespace std::chrono_literals;

int main() {
    auto start = cr::high_resolution_clock::now();
    {
        auto serverChannel = std::promise<int>{};
        auto clientChannel = serverChannel.get_future();
        {
            auto server = std::jthread{[&serverChannel] {
                cout << "[server] started\n";
                std::this_thread::sleep_for(2s);
                try {
                    auto txt = std::string{"ABC"};
                    char c = txt.at(42);
                } catch (std::out_of_range& x) {
                    cout << "[server] error: " << x.what() << "\n";
                    serverChannel.set_exception(std::current_exception());
                }
            }};
        }
        try {
            auto result = clientChannel.get();
            cout << "[main] answer = " << result << "\n";
        } catch (std::exception& x) {
            cout << "[main] failed = " << x.what() << "\n";
        }
    }
    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(end - start);
    cout << "[main] elapsed time: " << elapsed.count() << " ms\n";
}

