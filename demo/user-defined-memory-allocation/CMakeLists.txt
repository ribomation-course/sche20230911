cmake_minimum_required(VERSION 3.22)
project(user_defined_memory_allocation)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(user_defined_memory_allocation prod-cons.cxx)
target_compile_options(user_defined_memory_allocation PRIVATE ${WARN})


