#pragma once

#include <functional>
#include <array>
#include <pthread.h>

namespace ribomation::concurrent {

    class Mutex {
        mutable pthread_mutex_t mutex{};
    public:
        Mutex() {
            pthread_mutexattr_t attrs;
            pthread_mutexattr_init(&attrs);
            pthread_mutexattr_setpshared(&attrs, PTHREAD_PROCESS_SHARED);
            pthread_mutex_init(&mutex, &attrs);
            pthread_mutexattr_destroy(&attrs);
        }

        ~Mutex() { pthread_mutex_destroy(&mutex); }

        void lock() const { pthread_mutex_lock(&mutex); }
        void unlock() const { pthread_mutex_unlock(&mutex); }
        auto native() const { return &mutex; }
    };

    class LockGuard {
        Mutex& mutex;
    public:
        explicit LockGuard(Mutex& mutex) : mutex(mutex) { mutex.lock(); }

        ~LockGuard() { mutex.unlock(); }
    };

    class Condition {
        mutable pthread_cond_t condition{};
        Mutex& mutex;
    public:
        explicit Condition(Mutex& mutex) : mutex(mutex) {
            pthread_condattr_t attrs{};
            pthread_condattr_init(&attrs);
            pthread_condattr_setpshared(&attrs, PTHREAD_PROCESS_SHARED);
            pthread_cond_init(&condition, &attrs);
            pthread_condattr_destroy(&attrs);
        }

        ~Condition() { pthread_cond_destroy(&condition); }

        void wait(std::function<bool()> const& ok_to_proceed) {
            while (!ok_to_proceed()) {
//                pthread_cond_wait(&condition, mutex.native());
                auto timeout = timespec{.tv_sec=0, .tv_nsec=1'000'000};
                pthread_cond_timedwait(&condition, mutex.native(), &timeout);
            }
        }

        void notify_one() { pthread_cond_signal(&condition); }
        void notify_all() { pthread_cond_broadcast(&condition); }
    };

    template<typename PayloadType, unsigned CAPACITY = 128>
    class MessageQueue {
        std::array<PayloadType, CAPACITY> queue{};
        unsigned size = 0, putIx = 0, getIx = 0;
        mutable Mutex exclusive{};
        Condition not_empty{exclusive};
        Condition not_full{exclusive};

        bool empty() const { return size == 0; }
        bool full() const { return size == CAPACITY; }

    public:
        void put(PayloadType x) {
            auto g = LockGuard{exclusive};
            not_full.wait([this] { return not full(); });
            queue[putIx] = x;
            putIx = (putIx + 1) % CAPACITY;
            ++size;
            not_empty.notify_all();
        }

        auto get() -> PayloadType {
            auto g = LockGuard{exclusive};
            not_empty.wait([this] { return not empty(); });
            PayloadType x = queue[getIx];
            getIx = (getIx + 1) % CAPACITY;
            --size;
            not_full.notify_all();
            return x;
        }

        auto count() const {
            auto g = LockGuard{exclusive};
            return size;
        }
        static auto capacity() { return CAPACITY; }
    };

}
