#pragma once

#include <string>
#include <stdexcept>
#include <utility>
#include <cerrno>
#include <cmath>
#include <cstring>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

namespace ribomation::concurrent {
    using namespace std::string_literals;

    class SharedMemory {
        const size_t capacity;
        const std::string name;
        void* storage;
        void* next;

        static size_t alignSize(size_t capacity) {
            auto const P = getpagesize();
            auto const C = static_cast<double>(capacity);
            return static_cast<size_t>(P * ceil(C / P));
        }

        static void* mkShm(size_t capacity, std::string const& name) {
            auto shmFd = shm_open(name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0600);
            if (shmFd == -1)
                throw std::runtime_error{"shm_open failed: "s + ::strerror(errno)};

            if (ftruncate(shmFd, static_cast<__off_t>(capacity)) == -1)
                throw std::runtime_error{"ftruncate failed: "s + ::strerror(errno)};

            auto shm = mmap(nullptr, capacity, PROT_READ | PROT_WRITE, MAP_SHARED, shmFd, 0);
            if (shm == MAP_FAILED)
                throw std::runtime_error{"mmap failed: "s + ::strerror(errno)};
            close(shmFd);

            return shm;
        }

    public:
        explicit SharedMemory(size_t const capacity_, std::string name_ = "/cxx_shm"s)
                : capacity{alignSize(capacity_)}, name{std::move(name_)} {
            storage = mkShm(capacity, name);
            next = storage;
        }

        ~SharedMemory() {
            munmap(storage, capacity);
            shm_unlink(name.c_str());
        }

        void* allocate(size_t bytes) {
            void* addr = next;
            next = reinterpret_cast<char*>(next) + bytes;
            if ((reinterpret_cast<char*>(storage) + capacity) <= next)
                throw std::overflow_error{"shared memory overflow"s};
            return addr;
        }

        template<typename T>
        T* allocate(unsigned numElems = 1) {
            return reinterpret_cast<T*>(allocate(numElems * sizeof(T)));
        }
    };

}

