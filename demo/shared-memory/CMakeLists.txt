cmake_minimum_required(VERSION 3.22)
project(shared_memory)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(fibonacci
        shared-memory.hxx
        fibonacci.cxx)
target_compile_options(fibonacci PRIVATE ${WARN})
target_link_libraries(fibonacci PRIVATE rt)

add_executable(producer-consumer
        shared-memory.hxx
        mutex.hxx
        condition.hxx
        mailbox.hxx
        producer-consumer.cxx)
target_compile_options(producer-consumer PRIVATE ${WARN})
target_link_libraries(producer-consumer PRIVATE rt)

