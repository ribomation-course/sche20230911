#include <iostream>
#include <array>
#include <cstdlib>
#include <sys/wait.h>
#include "shared-memory.hxx"

namespace rm = ribomation::concurrent;
using std::cout;

struct Data {
    unsigned arg{};
    unsigned long result{};
};
auto operator <<(std::ostream& os, Data const& d) -> std::ostream& {
    return os << "fib(" << d.arg << ") = " << d.result;
}

auto fibonacci(unsigned n) {
    auto f0 = 0UL, f1=1UL;
    while (n-- > 0) {
        auto f = f0 + f1;
        f0 = f1;
        f1 = f;
    }
    return f0;
}

int main() {
    auto const N = 90U;
    using shm_array = std::array<Data, N>;
    auto shm = rm::SharedMemory{sizeof(shm_array)};
    auto data = shm.allocate<shm_array>();

    auto rc = fork();
    if (rc == -1) return 1;
    if (rc == 0) {
        auto arg = 1U;
        for (auto& d : *data) {
            d = {arg, fibonacci(arg)};
            ++arg;
        }
        cout << "[child] done\n";
        exit(0);
    }

    wait(nullptr);
    cout << "[parent] printing results\n";
    for (auto&& d : *data) cout << d << "\n";
}
