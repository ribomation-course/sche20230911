#include <iostream>
#include <vector>
#include <syncstream>
#include <thread>
#include <semaphore>
#include <latch>

struct sem_guard {
    std::binary_semaphore& sem;
    explicit sem_guard(std::binary_semaphore& sem) : sem(sem) { sem.acquire(); }
    ~sem_guard() { sem.release(); }
};

class Account {
    int balance = 0;
    mutable std::binary_semaphore exclusive{1};
public:
    Account() = default;
    Account(Account const&) = delete;
    auto operator=(Account const&) -> Account& = delete;

    void update(int amount) {
        auto guard = sem_guard{exclusive};
        balance += amount;
    }

    int getBalance() const {
        auto guard = sem_guard{exclusive};
        return balance;
    }
};

unsigned num_updaters = 10U;
unsigned num_transactions = 100'000U;
int amount = 100;

void args(int argc, char** argv) {
    using namespace std::string_literals;
    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "-u"s) num_updaters = std::stoi(argv[++k]);
        else if (arg == "-t"s) num_transactions = std::stoi(argv[++k]);
        else if (arg == "-a"s) amount = std::stoi(argv[++k]);
        else {
            std::cerr << "usage: " << argv[0] << " [-u <int>] [-t <int>] [-a <int>]\n";
            throw std::invalid_argument{"option "s + arg};
        }
    }
    using std::cout;
    cout << "# updaters    : " << num_updaters << "\n";
    cout << "# transactions: " << num_transactions << "\n";
    cout << "amount        : " << amount << " kr\n";
}

void run() {
    auto the_account = Account{};
    std::cout << "[main] initial balance: " << the_account.getBalance() << " kr\n";

    auto proceed = std::latch{num_updaters};
    auto updater = [&the_account, &proceed](unsigned id) {
        proceed.arrive_and_wait();
        std::osyncstream{std::cout} << "updater-" << id << " started\n";

        for (auto k = 0U; k < num_transactions; ++k) the_account.update(+amount);
        std::this_thread::sleep_for(std::chrono::milliseconds{500});
        for (auto k = 0U; k < num_transactions; ++k) the_account.update(-amount);

        std::osyncstream{std::cout} << "updater-" << id << " done: balance= " << the_account.getBalance() << " kr\n";
    };

    {
        auto updaters = std::vector<std::jthread>{};
        updaters.reserve(num_updaters);
        for (auto id = 1U; id <= num_updaters; ++id) updaters.emplace_back(updater, id);
    }
    std::cout << "[main] final balance: " << the_account.getBalance() << " kr\n";
}

int main(int argc, char** argv) {
    args(argc, argv);
    run();
}

