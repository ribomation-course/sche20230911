#include <iostream>
#include <string>
#include <thread>
#include <syncstream>
#include <vector>
#include <memory>
#include "receivable.hxx"

namespace rm = ribomation::concurrent;
using namespace std::string_literals;
using std::cout;
using std::osyncstream;

auto const TAB = 5U;

struct Producer {
    unsigned const N;
    rm::Receivable<long>* next = nullptr;

    Producer(unsigned n, rm::Receivable<long>* next) : N(n), next(next) {}

    void run() const {
        for (auto msg = 1U; msg <= N; ++msg) {
            osyncstream{cout} << "[prod] msg:" << msg << "\n";
            next->send(msg);
        }
        next->send(0);
    }
};

struct Consumer : rm::Receivable<long> {
    unsigned const T;

    explicit Consumer(unsigned t) : T(t) {}

    void run() {
        auto const prefix = std::string((T + 1) * TAB, ' ') + "[cons] ";
        for (auto msg = recv(); msg != 0; msg = recv()) {
            osyncstream{cout} << prefix << msg << "\n";
        }
        osyncstream{cout} << prefix << "done\n";
    }
};

struct Transformer : rm::Receivable<long> {
    unsigned const id;
    rm::Receivable<long>* next = nullptr;

    explicit Transformer(unsigned id_, rm::Receivable<long>* next) : id{id_}, next{next} {}

    void run() {
        auto const prefix = std::string(id * TAB, ' ') + "[tran-" + std::to_string(id) + "] ";
        for (auto msg = recv(); msg != 0; msg = recv()) {
            auto result = 2 * msg;
            osyncstream{cout} << prefix << msg << " --> " << result << "\n";
            next->send(result);
        }
        next->send(0);
        osyncstream{cout} << prefix << "done\n";
    }
};


int main(int argc, char* argv[]) {
    auto N = 100U;
    auto T = 3U;

    for (int k = 1; k < argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "-n"s) {
            N = std::stoi(argv[++k]);
        } else if (arg == "-t"s) {
            T = std::stoi(argv[++k]);
        }
    }

    auto transformers = std::vector<std::unique_ptr<Transformer>>{};
    transformers.reserve(T);

    auto threads = std::vector<std::jthread>{};
    threads.reserve(T + 2);

    auto consumer = Consumer{T};
    threads.emplace_back(&Consumer::run, &consumer);

    rm::Receivable<long>* next = &consumer;
    for (auto id = T; id > 0; --id) {
        auto transformer = new Transformer{id, next};
        transformers.emplace_back(transformer);
        threads.emplace_back(&Transformer::run, transformer);
        next = transformer;
    }

    auto producer = Producer{N, next};
    threads.emplace_back(&Producer::run, &producer);

    cout << "[main] done\n";
}
