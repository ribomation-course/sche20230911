cmake_minimum_required(VERSION 3.25)
project(ch14_thread_pools)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(ch14_thread_pools
        message-queue-unbounded.hxx
        thread-pool-result.hxx
        phrase-count.cxx)
target_compile_options(ch14_thread_pools PRIVATE ${WARN})


