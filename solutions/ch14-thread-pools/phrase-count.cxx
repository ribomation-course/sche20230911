#include <iostream>
#include <fstream>
#include <filesystem>
#include <string_view>
#include <tuple>
#include <vector>
#include <numeric>
#include <memory>
#include <future>
#include <chrono>
#include <functional>
#include "thread-pool-result.hxx"

namespace co = ribomation::concurrent;
namespace fs = std::filesystem;
namespace cr = std::chrono;
using namespace std::literals;
using std::string;
using std::string_view;
using std::unique_ptr;
using std::tuple;
using std::cout;

auto load(string const& filename) -> tuple<string_view, unique_ptr<char>>;
auto count(unsigned N, string_view payload, string_view phrase) -> unsigned;
void benchmark(const std::function<void()>&);

int main(int argc, char* argv[]) {
    auto filename = "../files/shakespeare.txt"s;
    auto phrase = "Hamlet"sv;
    auto N = 25U;

    for (auto k = 1; k < argc; ++k) {
        if (argv[k] == "-f"s) filename = argv[++k];
        else if (argv[k] == "-p"s) phrase = argv[++k];
        else if (argv[k] == "-n"s) N = std::stoi(argv[++k]);
    }

    benchmark([=] {
        auto [payload, buf] = load(filename);
        auto total = count(N, payload, phrase);
        cout << "[main] total phrase count = " << total << "\n";
    });
}

auto load(const string& filename) -> tuple<std::string_view, std::unique_ptr<char>> {
    auto size = fs::file_size(filename);
    auto buf = new char[size];
    auto f = std::ifstream{filename};
    f.read(buf, static_cast<long>(size));
    auto payload = std::string_view{buf, size};
    return {payload, std::unique_ptr<char>{buf}};
}

auto count(unsigned int N, std::string_view payload, std::string_view phrase) -> unsigned {
    auto pool = co::ThreadPoolResult<unsigned>{};
    auto results = std::vector<std::shared_future<unsigned>>{};
    results.reserve(N);
    auto chunkSize = payload.size() / N;
    for (auto k = 0U; k < N; ++k) {
        auto chunk = payload.substr(k * chunkSize, chunkSize);
        results.push_back(pool.submit([chunk, phrase] {
            auto count = 0U;
            for (auto start = chunk.find(phrase);
                 start != std::string_view::npos;
                 start = chunk.find(phrase, start + phrase.size() + 1)) {
                ++count;
            }
            return count;
        }));
    }

    std::cout << "[main] before results aggregation\n";
    return std::accumulate(results.begin(), results.end(), 0U,
                           [](auto sum, auto& partial) {
                               return sum + partial.get();
                           });
}

void benchmark(std::function<void()> const& stmts) {
    auto start = cr::high_resolution_clock::now();

    stmts();

    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(end - start);
    cout << "[main] elapsed time = " << elapsed.count() << " ms\n";
}
