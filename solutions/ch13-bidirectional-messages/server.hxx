#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>
#include "receivable.hxx"
#include "rendezvous.hxx"

namespace ribomation::concurrent {
    using namespace std::string_literals;
    using std::string;
    using Message = RendezvousMessage<string, string>;

    auto toUpper(string s) {
        std::transform(s.begin(), s.end(), s.begin(), [](char ch) {
            return ::toupper(ch);
        });
        return s;
    }

    struct Server : Receivable<Message*> {
        bool done = false;

        auto compute(string const& arg) -> string {
            auto msg = Message{arg};
            put(&msg);
            return msg.promise.get_future().get();
        }

        void run() {
            do {
                auto msg = get();
                auto str = msg->payload;
                if (done && str.empty()) {
                    msg->promise.set_value(""s);
                    break;
                }
                msg->promise.set_value(toUpper(str));
            } while (true);
            std::cout << "[server] done\n";
        }
    };

}
