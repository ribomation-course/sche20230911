#pragma once
#include "message-queue-unbounded.hxx"

namespace ribomation::concurrent {

    template<typename MessageType>
    struct Receivable {
    private:
        MessageQueueUnbounded<MessageType> inbox;

    protected:
        void put(MessageType msg) { inbox.put(msg); }
        MessageType get() { return inbox.get(); }

    public:
        Receivable() = default;
        virtual ~Receivable() = default;
        Receivable(Receivable const&) = delete;
        auto operator =(Receivable const&) -> Receivable& = delete;
    };

}
