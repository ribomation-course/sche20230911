#include <iostream>
#include <string>
#include <thread>
#include "server.hxx"
#include "client.hxx"

namespace co = ribomation::concurrent;
using namespace std::string_literals;
using std::cout;

int main(int argc, char* argv[]) {
    auto N = 100U;
    auto filename1 = "./files/musketeers.txt"s;
    auto filename2 = "./files/shakespeare.txt"s;

    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "-n"s) {
            N = std::stoi(argv[++k]);
        } else if (arg == "-f1"s) {
            filename1 = argv[++k];
        } else if (arg == "-f2"s) {
            filename2 = argv[++k];
        } else {
            cout << "Usage: " << argv[0] << " [-n N] [-f1 FILE1] [-f2 FILE2]\n";
            return 1;
        }
    }

    auto server = co::Server{};
    auto serverThr = std::jthread{&co::Server::run, &server};

    {
        auto client1 = co::Client{1, filename1, N, server};
        auto client1Thr = std::jthread{&co::Client::run, &client1};

        auto client2 = co::Client{2, filename2, N, server};
        auto client2Thr = std::jthread{&co::Client::run, &client2};
    }

    cout << "[main] before done\n";
    server.done = true;
    server.compute(""s);
    cout << "[main] done\n";
}
