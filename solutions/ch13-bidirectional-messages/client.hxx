#pragma once
#include <iostream>
#include <string>
#include <syncstream>
#include "server.hxx"
#include "file-slurper.hxx"

namespace ribomation::concurrent {
    using std::string;
    using std::cout;
    using std::osyncstream;

    struct Client {
        unsigned id;
        string const filename;
        unsigned maxLines;
        Server& server;

        Client(unsigned id, string  filename, unsigned maxLines, Server& server)
                : id(id), filename(std::move(filename)), maxLines(maxLines), server(server) {}

        void run() const {
            osyncstream{cout} << "[client-" << id << "] started\n";

            decltype(maxLines) lineno{};
            for (auto line: io::FileSlurper{filename, maxLines}) {
                if (line.empty()) { ++lineno; continue; }
                osyncstream{cout} << "[client-" << id << "] ("<< ++lineno <<") \"" << server.compute(line) << "\"\n";
            }

            osyncstream{cout} << "[client-" << id << "] done\n";
        }
    };


}