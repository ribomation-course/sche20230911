cmake_minimum_required(VERSION 3.25)
project(ch17_user_defined_memory_alloc)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(user_defined_memory_alloc app.cxx)
target_compile_options(user_defined_memory_alloc PRIVATE ${WARN})


