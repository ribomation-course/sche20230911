#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <vector>
#include <thread>
#include <latch>
#include <syncstream>

namespace cr = std::chrono;
using std::cout;
using namespace std::string_literals;
const auto MESSAGE = "School's out, it's summer!"s;

void SLEEP() {
    static auto R = std::random_device{};
    static auto r = std::default_random_engine{R()};
    static auto sleep_time = std::uniform_int_distribution<int>{1000, 4000}(r);
    std::this_thread::sleep_for(cr::milliseconds(sleep_time));
}

int main(int argc, char* argv[]) {
    auto W = 10U;
    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "--workers"s) {
            W = std::stoi(argv[++k]);
        }
    }

    auto done = std::latch{W};

    auto worker = [&done](unsigned id) {
        auto const tab = std::string(id * 3, ' ');
        std::osyncstream{cout} << tab << "[" << id << "] CREATE\n";
        SLEEP();

        std::osyncstream{cout} << tab << "WAIT\n";
        done.arrive_and_wait();

        std::osyncstream{cout} << tab << "DONE\n";
    };

    std::osyncstream{cout} << "[Main] Creating workers...\n";
    {
        auto workers = std::vector<std::jthread>{};
        workers.reserve(W);
        for (auto id = 1U; id <= W; ++id) workers.emplace_back(worker, id);

        std::osyncstream{cout} << "[Main] before wait()...\n";
        done.wait();
    }

    std::osyncstream{cout} << "[Main] " << MESSAGE << "\n";
}
