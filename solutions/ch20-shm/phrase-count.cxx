#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <filesystem>
#include <numeric>
#include <syncstream>
#include <sys/wait.h>
#include "shared-memory.hxx"

using namespace std::string_literals;
using namespace std::string_view_literals;
namespace fs = std::filesystem;
namespace rm = ribomation::concurrent;
using std::cout;
using std::string;
using std::string_view;
using std::osyncstream;

unsigned count(string_view chunk, string_view phrase) {
    unsigned cnt = 0;
    for (auto it = chunk.find(phrase);
         it != string_view::npos;
         it = chunk.find(phrase, it + phrase.size())) {
        ++cnt;
    }
    return cnt;
}

void child(unsigned id, string_view chunk, string_view phrase, unsigned* counts) {
    auto const tab = string(id * 3, ' ');
    osyncstream{cout} << tab << "worker-" << id << " started\n";
    counts[id] = count(chunk, phrase);
    osyncstream{cout} << tab << "worker-" << id << " count=" << counts[id] << "\n";
    exit(0);
}

auto load(string const& filename, unsigned long fsize, rm::SharedMemory& shm) -> string_view {
    auto fileStorage = reinterpret_cast<char*>(shm.allocate(fsize));
    auto file = std::ifstream{filename};
    if (!file) {
        throw std::invalid_argument{"cannot open "s + filename};
    }
    file.read(fileStorage, static_cast<long>(fsize));
    return string_view{fileStorage, fsize};
}

int main(int argc, char* argv[]) {
    auto numWorkers = 6U;
    auto filename = "../shakespeare.txt"s;
    auto phrase = "Hamlet"sv;

    for (auto k = 1; k < argc; ++k) {
        if (argv[k] == "-w"s) {
            numWorkers = std::stoul(argv[++k]);
        } else if (argv[k] == "-f"s) {
            filename = argv[++k];
        } else if (argv[k] == "-p"s) {
            phrase = argv[++k];
        }
    }

    auto fsize = fs::file_size(filename);
    auto shm = rm::SharedMemory{fsize + numWorkers * sizeof(unsigned)};
    auto payload = load(filename, fsize, shm);
    auto counts = new(shm.allocate<unsigned>(numWorkers)) unsigned[numWorkers]{};

    auto chunkSize = fsize / numWorkers;
    for (auto k = 0U; k < numWorkers; ++k) {
        if (fork() == 0) {
            child(k + 1, payload.substr(k * chunkSize, chunkSize), phrase, counts);
        }
    }
    for (auto id = 0U; id < numWorkers; ++id) ::wait(nullptr);

    auto cnt = std::accumulate(&counts[0], &counts[numWorkers], 0U);
    cout << "result: " << phrase << ": " << cnt << "\n";
}
