cmake_minimum_required(VERSION 3.25)
project(ch18_pmr)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(pmr pmr-words.cxx)
target_compile_options(pmr PRIVATE ${WARN})
