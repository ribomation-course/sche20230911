#include <iostream>
#include <memory_resource>
#include <set>
#include <string>
#include <algorithm>
#include <iterator>

using byte = unsigned char;
namespace m = std::pmr;
namespace r = std::ranges;
using std::cout;

void* operator new(size_t) { throw std::bad_alloc{}; }
void* operator new[](size_t) { throw std::bad_alloc{}; }

int main() {
    byte storage[15'000]{};
    auto const pad = '\044'; //dollar
    r::fill_n(std::data(storage), std::size(storage), pad);
    auto buffer = m::monotonic_buffer_resource{std::data(storage), std::size(storage), m::null_memory_resource()};
    auto pool = m::unsynchronized_pool_resource{&buffer};

    auto words = m::set<m::string>{&pool};
    for (auto chars = m::string{&pool}; std::cin >> chars;) words.insert(chars);
    for (auto&& w: words) cout << w << ' ';
    cout << '\n';

    cout << "--------------------------\n";
    auto firstUnusedByte = std::find_if(std::rbegin(storage), std::rend(storage), [](auto x) { return x != pad; });
    auto used = (unsigned long) (&*firstUnusedByte) - (unsigned long) storage;
    auto free = std::size(storage) - used;
    cout << "Memory: used=" << used << " bytes, free=" << free << " bytes, total=" << std::size(storage) << " bytes\n";

    try {
        auto dummy = std::string{"1234567890123456"};
        cout << "Failure; system heap might have been used";
    } catch (std::exception const& x) {
        cout << "Confirmed; no system heap space used\n";
    }
}
