#include <iostream>
#include <fstream>
#include <string>
#include <syncstream>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <utility>

class monitor {
    std::mutex exclusive;
    std::condition_variable event;
public:
    virtual ~monitor() = default;
protected:
    struct Context {
        monitor& self;
        std::unique_lock<std::mutex>& lck;
        Context(monitor& self, std::unique_lock<std::mutex>& lck) : self(self), lck(lck) {}
        void wait(std::function<bool()> ready) {
            self.event.wait(lck, std::move(ready));
        }
        void notify() {
            self.event.notify_all();
        }
    };

    void synchronized(std::function<void(Context&)> const& stmts) {
        auto guard = std::unique_lock<std::mutex>{exclusive};
        auto ctx = Context{*this, guard};
        stmts(ctx);
    }
};

template<typename PayloadType>
class Transfer : protected monitor {
    PayloadType payload{};
    bool full{};
public:
    void send(decltype(payload) x) {
        synchronized([this, &x](Context& ctx) {
            ctx.wait([this]() { return not full; });
            payload = x;
            full = true;
            ctx.notify();
        });
    }

    auto recv() -> decltype(payload) {
        auto x = decltype(payload){};
        synchronized([this, &x](Context& ctx) {
            ctx.wait([this]() { return full; });
            x = payload;
            full = false;
            ctx.notify();
        });
        return x;
    }
};

int main() {
    using namespace std::string_literals;
    auto tx = Transfer<std::string>{};

    {
        auto receiver = std::jthread{[&tx]() {
            auto lineno = 1U;
            for (auto msg = tx.recv(); msg != "__END__"s; msg = tx.recv()) {
                std::osyncstream{std::cout} << "[recv] " << lineno++ << ") " << msg << "\n";
            }
            std::osyncstream{std::cout} << "[recv] DONE\n";
        }};

        auto file = std::ifstream{"./app.cxx"};
        if (!file) {
            throw std::invalid_argument{"cannot open ./app.cxx"s};
        }

        for (std::string line; std::getline(file, line); ) {
            tx.send(line);
        }
        tx.send("__END__");
    }

    std::cout << "[main] DONE\n";
}

