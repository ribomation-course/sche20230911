#include <iostream>
#include <thread>
#include "message-queue.hxx"

int main() {
    auto const N = 1000U;
    auto Q = ribomation::concurrent::MessageQueueBounded<unsigned, 32>{};

    auto consumer = std::jthread{[&Q] {
        for (auto msg = Q.get(); msg != 0; msg = Q.get()) {
            std::cout << "Consumer: " << msg << std::endl;
        }
    }};
    {
        auto producer = [&Q](unsigned start) {
            for (auto k = start; k <= N; k += 2) Q.put(k);
        };
        auto odd = std::jthread{producer, 1U};
        auto even = std::jthread{producer, 2U};
    }
    Q.put(0);
}
