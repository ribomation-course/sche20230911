#include <iostream>
#include <fstream>
#include <filesystem>
#include <string_view>
#include <vector>
#include <numeric>
#include <future>

namespace fs = std::filesystem;
using namespace std::literals;

int main() {
    auto const N = 4U;
    auto filename = "./hemsoborna.txt"s;
    auto size = fs::file_size(filename);
    char buf[size];
    {
        auto file = std::ifstream{filename};
        file.read(buf, static_cast<long>(size));
    }

    auto payload = std::string_view{buf, size};
    auto const chunk_size = size / N;
    auto results = std::vector<std::future<unsigned >>{};
    for (auto k = 0U; k < N; ++k) {
        auto chunk = payload.substr(k * chunk_size, chunk_size);

        auto result = std::async([chunk]() {
            auto cnt = 0U;
            for (auto start = 0U; start < chunk.size(); ++start) {
                auto end = chunk.find(' ', start);
                if (end == std::string_view::npos) break;
                ++cnt;
                start = end;
            }
            return cnt;
        });

        results.push_back(std::move(result));
    }

    auto total = std::accumulate(std::begin(results), std::end(results), 0U,
                                 [](auto sum, auto& f) { return sum + f.get(); });
    std::cout << "Total spaces: " << total << "\n";
}
