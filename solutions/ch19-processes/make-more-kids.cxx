#include <iostream>
#include <vector>
#include <stdexcept>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

class Process {
private:
    int pid{};
protected:
    virtual void run() = 0;

public:
    Process() = default;

    virtual ~Process() {
        join();
    }

    void start() {
        auto rc = ::fork();
        if (rc < 0) {
            throw std::runtime_error{"fork() failed"};
        }
        pid = rc;
        if (rc > 0) return;
        run();
        exit(0);
    }

    void join() const {
        if (pid == 0) return;
        auto rc = ::waitpid(pid, nullptr, 0);
        if (rc < 0) {
            throw std::runtime_error{"waitpid() failed"};
        }
    }
};


class Kid : public Process {
    unsigned N;
public:
    explicit Kid(unsigned N) : N(N) {
        //start();
    }

protected:
    void run() override {
        auto pid = ::getpid();
        for (auto k = 0U; k < N; ++k) {
            std::cout << "Hello from child " << pid << "\n";
            usleep(10 * 1000);
        }
        std::cout << "Child " << pid << " done\n";
    }
};

int main(int argc, char* argv[]) {
    using namespace std::string_literals;
    auto P = 25;
    auto N = 10;

    for (auto k = 1; k < argc; ++k) {
        if (argv[k] == "-p"s) {
            P = std::stoi(argv[k + 1]);
        } else if (argv[k] == "-n"s) {
            N = std::stoi(argv[k + 1]);
        }
    }

    auto processes = std::vector<Kid>{};
    for (auto k = 0; k < P; ++k) processes.emplace_back(N);
    for (auto& p : processes) p.start();

}
