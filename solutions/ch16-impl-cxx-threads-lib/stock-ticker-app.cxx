#include <iostream>
#include <string>
#include <random>
#include <unistd.h>
#include "thread.hxx"
#include "syncstream.hxx"
#include "read-write-lock.hxx"

namespace co = ribomation::concurrent;
using namespace std::literals;

class Ticker {
    long value{};
    mutable co::ReadWriteLock rwl{};
public:
    auto read() const {
        auto g = co::LockForReading{rwl};
        return value;
    }

    auto update(int amount) {
        auto g = co::LockForWriting{rwl};
        value += amount;
        return value;
    }
};

int main() {
    auto ticker = Ticker{};

    auto updater = co::Thread{[&ticker] {
        auto R = std::random_device{};
        auto amount = std::uniform_int_distribution<int>{-5, +5};
        for (;;) {
            sleep(3);
            co::osyncstream{std::cout} << "[writer] " << ticker.update(amount(R)) << "\n";
        }
    }};

    auto reader = [&ticker](int id) {
        auto tab = std::string(id * 5, ' ');
        for (;;) {
            co::osyncstream{std::cout} << tab << "[reader-" << id << "] " << ticker.read() << "\n";
            usleep(500 * 1000);
        }
    };

    auto r1 = co::Thread{reader, 1U};
    auto r2 = co::Thread{reader, 2U};
    auto r3 = co::Thread{reader, 3U};
}
