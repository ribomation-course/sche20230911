#include <iostream>
#include <string>
#include "thread.hxx"
#include "mutex.hxx"
#include "syncstream.hxx"

namespace co = ribomation::concurrent;
using std::cout;

class Account {
    int balance{};
    co::Mutex exclusive{};
public:
    int getBalance() const { return balance; }

    void update(int amount) {
        auto g = co::LockGuard{exclusive};
        balance += amount;
    }
};

int main() {
    auto const N = 10'000U;
    auto const A = 100;
    auto the_account = Account{};

    {
        auto updater = [&the_account] {
            co::osyncstream{cout} << "[updater] start: " << the_account.getBalance() << "\n";
            for (auto k = 0U; k < N; ++k) the_account.update(+A);
            for (auto k = 0U; k < N; ++k) the_account.update(-A);
            co::osyncstream{cout} << "[updater] done: " << the_account.getBalance() << "\n";
        };

        auto u1 = co::Thread{updater};
        auto u2 = co::Thread{updater};
        auto u3 = co::Thread{updater};
        auto u4 = co::Thread{updater};
        auto u5 = co::Thread{updater};
    }

    cout << "[main] final balance = " << the_account.getBalance() << "\n";
}
