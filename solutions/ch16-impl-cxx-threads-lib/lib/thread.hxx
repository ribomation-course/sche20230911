#pragma once

#include <functional>
#include <pthread.h>

namespace ribomation::concurrent {

    class Thread {
        pthread_t runner{};
        std::function<void()> body;

        static void* wrapper(Thread* t) {
            t->body();
            return nullptr;
        }

    public:
        template<typename BodyFn, typename... Args>
        explicit Thread(BodyFn bodyFn, Args... args) {
            body = std::move([bodyFn = std::move(bodyFn), args = std::make_tuple(std::move(args)...)] {
                std::apply(bodyFn, args);
            });
            pthread_create(&runner, nullptr, reinterpret_cast<void* (*)(void*)>(wrapper), this);
        }

        ~Thread() { join(); }

        void join() const { pthread_join(runner, nullptr); }
    };

}

