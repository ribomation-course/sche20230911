#pragma once

#include <iostream>
#include <sstream>

namespace ribomation::concurrent {

    struct osyncstream : std::ostringstream {
        std::ostream& out;

        explicit osyncstream(std::ostream& out) : out{out} {}

        ~osyncstream() override {
            std::cout << str() << std::flush;
        }
    };

}

