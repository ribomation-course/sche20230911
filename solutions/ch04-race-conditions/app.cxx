#include <iostream>
#include <syncstream>
#include <thread>
#include <mutex>
#include <condition_variable>

template<typename PayloadType>
class MailBox {
    std::mutex exclusive{};
    std::condition_variable not_empty{};
    std::condition_variable not_full{};
    PayloadType payload{};
    bool full{};

public:
    void send(PayloadType amount) {
        {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_full.wait(guard, [this] { return !full; });
            payload = amount;
            full = true;
        }
        not_empty.notify_one();
    }

    int recv() {
        decltype(payload) data{};
        {
            auto guard = std::unique_lock<std::mutex>{exclusive};
            not_empty.wait(guard, [this] { return full; });
            data = payload;
            payload = {};
            full = false;
        }
        not_full.notify_one();
        return data;
    }
};

int main() {
    auto mb = MailBox<long>{};
    auto recv = std::jthread{[&mb] {
        for (auto msg = mb.recv(); msg != -1L; msg = mb.recv()) {
            std::osyncstream{std::cout} << "[consumer] " << msg << "\n";
        }
        std::osyncstream{std::cout} << "[consumer] DONE\n";
    }};

    auto const N = 1'000L;
    for (auto k = 1L; k <= N; ++k) mb.send(k);
    mb.send(-1L);
    std::osyncstream{std::cout} << "[producer] DONE\n";
}
